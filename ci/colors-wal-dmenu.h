static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#f6e0cd", "#191410" },
	[SchemeSel] = { "#f6e0cd", "#4757A0" },
	[SchemeOut] = { "#f6e0cd", "#FDD1AC" },
};
